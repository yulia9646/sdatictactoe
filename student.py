#class student, has name and year
class Student:
  def __init__(self, name, year):
    self.name = name
    self.year = year
    self.grades = []

  def add_grade(self, grade):
    if type(grade) is Grade:
      self.grades.append(grade)
    else:
      pass

  def get_average(self, grades): #get average student score
    return sum(grades) / len(grades)

class Grade:
  minimum_passing = 65
  def __init__(self, score):
    self.score = score
  def is_passing(self, grade): #check if a grade is passing
    if grade > minimum_passing:
      return "You passed!"
    else:
      return "You failed!"
#instances of student class
roger = Student("Roger van der Weyden", 10)
sandro = Student("Sandro Botticelli", 12)
pieter = Student("Pieter Bruegel the Elder", 8)
pieter.add_grade(Grade(100))

